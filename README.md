# rails_build_pro_env

# リモートサーバーにsshログイン
ssh root@ipアドレス

# さくらVPSの場合
```
httpプロトコルを許可
# firewall-cmd --add-service=http --zone=public --permanent
success
httpsプロトコルを許可
# firewall-cmd --add-service=https --zone=public --permanent
success
ファイアウォールサービスを再起動
# systemctl restart firewalld
```

# 以下を参考
https://www.phusionpassenger.com/library/walkthroughs/deploy/ruby/ownserver/apache/oss/install_language_runtime.html
```
sudo yum install -y curl gpg gcc gcc-c++ make
```

# Install RVM
```
sudo gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
curl -sSL https://get.rvm.io | sudo bash -s stable
sudo usermod -a -G rvm `whoami`

if sudo grep -q secure_path /etc/sudoers; then sudo sh -c "echo export rvmsudo_secure_path=1 >> /etc/profile.d/rvm_secure_path.sh" && echo Environment variable installed; fi
```

# Install Ruby
rvm install ruby-2.4.4
rvm --default use ruby-2.4.4

ruby -v
ruby 2.4.0p0 (2016-12-24 revision 57164) [x86_64-linux]

# Install Bundler
gem install bundler --no-rdoc --no-ri

以下を参照
https://www.phusionpassenger.com/library/walkthroughs/deploy/ruby/ownserver/apache/oss/el7/install_passenger.html
# enable EPEL
```
sudo yum install -y epel-release yum-utils
sudo yum-config-manager --enable epel
sudo yum clean all && sudo yum update -y
```

# repair potential system issues
```
sudo yum update -y
date
```

# install Passenger packages
# Install various prerequisites
```
sudo yum install -y pygpgme curl ruby-devel
```

# Add our el7 YUM repository
sudo curl --fail -sSLo /etc/yum.repos.d/passenger.repo https://oss-binaries.phusionpassenger.com/yum/definitions/el-passenger.repo

# Install Passenger + Apache module
sudo yum install -y mod_passenger || sudo yum-config-manager --enable cr && sudo yum install -y mod_passenger

# restart Apache
sudo systemctl restart httpd

# Check Installaiton
sudo /usr/bin/passenger-config validate-install

# ユーザー追加
sudo adduser vpslunch
passwd vpslunch
# ユーザーをwheelグループに追加
usermod -G wheel vpslunch
groups vpslunch
vpslunch : vpslunch wheel

# Mariadb install
 vi /etc/yum.repos.d/MariaDB.repo

```
# MariaDB 10.3 CentOS repository list - created 2018-06-10 08:37 UTC
# http://downloads.mariadb.org/mariadb/repositories/
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.3/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
```
# sudo yum install -y MariaDB-server MariaDB-client
# /etc/init.d/mysql start
初期設定
# mysql_secure_installation
# chkconfig mysql on

# Install Git on the server
sudo yum install -y git

# Pull Code
```
sudo mkdir -p /var/www/lunchfriends
sudo chown vpslunch: /var/www/lunchfriends
cd /var/www/lunchfriends
sudo -u vpslunch -H git clone https://github.com/***.git code
```

# Preparing the app's environment
#  Login as the app's user
sudo -u vpslunch -H bash -l

# Install dependencies
```
sudo yum -y install mysql-devel MariaDB-shared
cd code
bundle install --deployment --without development test
```

# Configure database.yml
```
# vi ~/.bashrc
export DB_USER="*******"
export DB_PASSWORD="*****"
```
# Configure secrets.yml
```
# bundle exec rake secret

copy
# vi config/secrets.yml
```

# vi config/database.yml

# Compile Rails assets and run database migrations
bundle exec rake assets:precompile db:migrate RAILS_ENV=production

# ApacheとPassengerの設定
```
# passenger-config about ruby-command
アドミンユーザーに切り替え
# sudo su
```

# Apacheの設定ファイルを編集
```
# sudo vi /etc/httpd/conf.d/lunchfriends.conf

<VirtualHost *:80>
    ServerName yourserver.com

    # Tell Apache and Passenger where your app's 'public' directory is
    DocumentRoot /var/www/myapp/code/public

    PassengerRuby /path-to-ruby

    # Relax Apache security settings
    <Directory /var/www/myapp/code/public>
      Allow from all
      Options -MultiViews
      # Uncomment this if you're on Apache >= 2.4:
      #Require all granted
    </Directory>
</VirtualHost>
```

# Apacheの再起動
```
# sudo apachectl restart

テスト
#curl http://yourserver.com/
```

# Restart application
```
# passenger-config restart-app
```